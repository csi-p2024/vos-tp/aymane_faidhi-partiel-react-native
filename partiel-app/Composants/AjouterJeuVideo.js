import React, { useState } from 'react';
import { View, Text, TextInput, Button, Image, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import * as ImagePicker from 'expo-image-picker';

const AjouterJeuVideo = ({ onAjouterJeu }) => {
  const [nom, setNom] = useState('');
  const [prix, setPrix] = useState('');
  const [categorie, setCategorie] = useState('');
  const [vignette, setVignette] = useState(null);

  const handleAjouter = () => {
    if (nom && prix && categorie) {
      const formattedPrix = `${parseFloat(prix)}€`;
      const nouveauJeu = {
        id: Math.floor(Math.random() * 1000000), 
        nom,
        prix: formattedPrix,
        categorie,
        vignette: vignette ? vignette.uri : null,
      };
      onAjouterJeu(nouveauJeu);
      setNom('');
      setPrix('');
      setCategorie('');
      setVignette(null);
    } else {
      Alert.alert('Veuillez remplir tous les champs');
    }
  };

  const choisirImage = async () => {
    const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (!permissionResult.granted) {
      alert("Permission to access camera roll is required!");
      return;
    }

    const pickerResult = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      quality: 1,
    });

    if (!pickerResult.canceled) {
      console.log('Image URI: ', pickerResult.assets[0].uri); 
      setVignette(pickerResult.assets[0]);
    }
  };

  return (
    <View style={styles.conteneur}>
      <Text style={styles.titre}>Ajouter un nouveau jeu vidéo</Text>
      <TextInput
        style={styles.input}
        placeholder="Titre"
        value={nom}
        onChangeText={setNom}
      />
      <TextInput
        style={styles.input}
        placeholder="Tarif"
        value={prix}
        onChangeText={setPrix}
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        placeholder="Catégorie"
        value={categorie}
        onChangeText={setCategorie}
      />
      <TouchableOpacity style={styles.boutonImage} onPress={choisirImage}>
        <Text style={styles.texteBoutonImage}>Choisir une image</Text>
      </TouchableOpacity>
      {vignette && (
        <Image source={{ uri: vignette.uri }} style={styles.image} />
      )}
      <Button title="Ajouter +" onPress={handleAjouter} />
    </View>
  );
};

const styles = StyleSheet.create({
  conteneur: {
    padding: 20,
    backgroundColor: '#f0f0f0',
    borderRadius: 10,
    marginVertical: 20,
    alignItems: 'center',
  },
  titre: {
    fontSize: 18,
    marginBottom: 10,
  },
  input: {
    width: '100%',
    padding: 10,
    marginVertical: 5,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  boutonImage: {
    backgroundColor: '#007BFF',
    padding: 10,
    borderRadius: 5,
    marginVertical: 10,
  },
  texteBoutonImage: {
    color: '#fff',
  },
  image: {
    width: 100,
    height: 100,
    marginVertical: 10,
  },
});

export default AjouterJeuVideo;
