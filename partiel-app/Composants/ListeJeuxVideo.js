import React from 'react';
import { View, ScrollView } from 'react-native';
import JeuVideo from './JeuVideo';

const ListeJeuxVideo = ({ jeux, onSupprimerJeu, onVendreJeu }) => {
  return (
    <ScrollView>
      {jeux.map(jeu => (
        <JeuVideo key={jeu.id} jeu={jeu} onSupprimerJeu={onSupprimerJeu} onVendreJeu={onVendreJeu} />
      ))}
    </ScrollView>
  );
};

export default ListeJeuxVideo;
