import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

const JeuVideo = ({ jeu, onSupprimerJeu }) => {
  return (
    <View style={styles.article}>
      {jeu.vignette && <Image source={{ uri: jeu.vignette }} style={styles.image} />}
      <Text style={styles.id}>ID: {jeu.id}</Text>
      <Text style={styles.nom}>{jeu.nom}</Text>
      <Text style={styles.prix}>{jeu.prix}</Text>
      <Text style={styles.categorie}>{jeu.categorie}</Text>
      <TouchableOpacity style={styles.boutonSupprimer} onPress={() => onSupprimerJeu(jeu.id)}>
        <Text style={styles.texteBoutonSupprimer}>Supprimer</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  article: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    borderRadius: 15,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 5,
    width: '90%',
    alignSelf: 'center',
  },
  id: {
    fontSize: 14,
    color: '#666',
    marginBottom: 5,
  },
  nom: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  prix: {
    fontSize: 18,
    color: '#333',
    marginBottom: 5,
  },
  categorie: {
    fontSize: 16,
    color: '#aaa',
    marginBottom: 10,
  },
  image: {
    width: 150,
    height: 150,
    marginBottom: 10,
    borderRadius: 10,
  },
  boutonSupprimer: {
    backgroundColor: '#d32f2f',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
  },
  texteBoutonSupprimer: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default JeuVideo;
