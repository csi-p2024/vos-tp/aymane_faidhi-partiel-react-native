import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

const TrierJeuxVideo = ({ tri, setTri }) => {
  return (
    <View style={styles.conteneur}>
      <Text style={styles.titre}>Trier par Prix</Text>
      <View style={styles.boutons}>
        <TouchableOpacity
          style={[
            styles.bouton,
            tri === 'croissant' && styles.boutonActif
          ]}
          onPress={() => setTri('croissant')}
        >
          <Text style={styles.texteBouton}>Croissant</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.bouton,
            tri === 'decroissant' && styles.boutonActif
          ]}
          onPress={() => setTri('decroissant')}
        >
          <Text style={styles.texteBouton}>Décroissant</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  conteneur: {
    padding: 20,
    backgroundColor: '#f0f0f0',
    borderRadius: 10,
    marginVertical: 20,
    alignItems: 'center',
  },
  titre: {
    fontSize: 18,
    marginBottom: 10,
  },
  boutons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  bouton: {
    padding: 10,
    margin: 5,
    backgroundColor: '#ccc',
    borderRadius: 5,
    flex: 1,
    alignItems: 'center',
  },
  boutonActif: {
    backgroundColor: '#888',
  },
  texteBouton: {
    color: '#fff',
  },
});

export default TrierJeuxVideo;
