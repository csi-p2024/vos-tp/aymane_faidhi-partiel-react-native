import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

const categories = ["Toutes", "FPS", "Combat", "Sport", "Action-Aventure", "Voiture", "Survival horror"];

const FiltrerJeuxVideo = ({ categorie, setCategorie }) => {
  return (
    <View style={styles.conteneur}>
      <Text style={styles.titre}>Filtrer par Catégorie</Text>
      <View style={styles.boutons}>
        {categories.map(cat => (
          <TouchableOpacity
            key={cat}
            style={[
              styles.bouton,
              categorie === cat && styles.boutonActif
            ]}
            onPress={() => setCategorie(cat)}
          >
            <Text style={styles.texteBouton}>{cat}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  conteneur: {
    padding: 20,
    backgroundColor: '#f0f0f0',
    borderRadius: 10,
    marginVertical: 20,
    alignItems: 'center',
  },
  titre: {
    fontSize: 18,
    marginBottom: 10,
  },
  boutons: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  bouton: {
    padding: 10,
    margin: 5,
    backgroundColor: '#ccc',
    borderRadius: 5,
  },
  boutonActif: {
    backgroundColor: '#888',
  },
  texteBouton: {
    color: '#fff',
  },
});

export default FiltrerJeuxVideo;
