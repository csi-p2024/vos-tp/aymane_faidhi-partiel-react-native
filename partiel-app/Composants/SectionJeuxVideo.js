import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import ListeJeuxVideo from './ListeJeuxVideo';
import AjouterJeuVideo from './AjouterJeuVideo';
import Header from './Header';
import FiltrerJeuxVideo from './FiltrerJeuxVideo';
import TrierJeuxVideo from './TrierJeuxVideo';

const SectionJeuxVideo = ({ jeux }) => {
  const [listeJeux, setListeJeux] = useState(jeux);
  const [categorie, setCategorie] = useState('Toutes');
  const [tri, setTri] = useState('croissant');
  const pseudo = 'Yazexo';

  const ajouterJeu = (nouveauJeu) => {
    setListeJeux([...listeJeux, nouveauJeu]);
  };

  const supprimerJeu = (id) => {
    setListeJeux(listeJeux.filter(jeu => jeu.id !== id));
  };

  const jeuxFiltres = categorie === 'Toutes' ? listeJeux : listeJeux.filter(jeu => jeu.categorie === categorie);

  const jeuxTries = [...jeuxFiltres].sort((a, b) => {
    const prixA = parseFloat(a.prix.replace('€', ''));
    const prixB = parseFloat(b.prix.replace('€', ''));
    return tri === 'croissant' ? prixA - prixB : prixB - prixA;
  });

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <Header pseudo={pseudo} nombreJeux={listeJeux.length} />
      <FiltrerJeuxVideo categorie={categorie} setCategorie={setCategorie} />
      <TrierJeuxVideo tri={tri} setTri={setTri} />
      <Text style={styles.entete}>Liste des jeux vidéos</Text>
      <View style={styles.listeContainer}>
        <ListeJeuxVideo jeux={jeuxTries} onSupprimerJeu={supprimerJeu} />
      </View>
      <View style={styles.ajouterJeuContainer}>
        <AjouterJeuVideo onAjouterJeu={ajouterJeu} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  entete: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
  },
  listeContainer: {
    width: '100%',
    marginBottom: 30,  
  },
  ajouterJeuContainer: {
    width: '100%',
  },
});

export default SectionJeuxVideo;
