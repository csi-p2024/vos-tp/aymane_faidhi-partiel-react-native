import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = ({ pseudo, nombreJeux }) => {
  return (
    <View style={styles.conteneur}>
      <Text style={styles.pseudo}>{pseudo}</Text>
      <Text style={styles.nombreJeux}>Nombre de jeux : {nombreJeux}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  conteneur: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#ddd',
    width: '100%',
  },
  pseudo: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  nombreJeux: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default Header;
