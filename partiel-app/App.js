import React, { useState } from 'react';
import SectionJeuxVideo from './Composants/SectionJeuxVideo';

export default function App() {
  const [jeux, setJeux] = useState([
    { id: 23124, nom: "Medal of Honor", prix: "10€", categorie: "FPS", vignette: "https://image.jeuxvideo.com/images/p2/m/o/mohfp20f.jpg" },
    { id: 12349, nom: "Street Fighter 2", prix: "20€", categorie: "Combat", vignette: "https://upload.wikimedia.org/wikipedia/en/1/1d/SF2_JPN_flyer.jpg" },
    { id: 549762, nom: "Call of Duty", prix: "30€", categorie: "FPS", vignette: "https://m.media-amazon.com/images/I/61I+6nlpvNS.jpg" },
    { id: 549763, nom: "NBA2K5", prix: "5€", categorie: "Sport", vignette: "https://cdn.gamekult.com/images/photos/00/00/53/83/ME0000538361_2.jpg" },
    { id: 549764, nom: "God Of War 2018", prix: "25€", categorie: "Action-Aventure", vignette: "https://image.api.playstation.com/vulcan/img/rnd/202010/2217/LsaRVLF2IU2L1FNtu9d3MKLq.jpg" },
    { id: 549765, nom: "The Legend of Zelda : The Wind Walker", prix: "35€", categorie: "Action-Aventure", vignette: "https://www.cdiscount.com/pdt2/3/1/0/3/700x700/nin0045496332310/rw/the-legend-of-zelda-the-wind-waker-hd-import.jpg" },
    { id: 549766, nom: "Horizon : Forbidden West", prix: "40€", categorie: "Action-Aventure", vignette: "https://image.api.playstation.com/vulcan/ap/rnd/202107/3100/kL1w4BZEWVcVbzugxJh6iYxC.png" },
    { id: 549767, nom: "Forza Horizon 5", prix: "45€", categorie: "Voiture", vignette: "https://shared.akamai.steamstatic.com/store_item_assets/steam/apps/1551360/capsule_616x353.jpg?t=1717876415" },
    { id: 549768, nom: "The Last Of Us", prix: "55€", categorie: "Survival horror", vignette: "https://static.posters.cz/image/750/affiches-et-posters/the-last-of-us-key-art-i127761.jpg" },
    { id: 549769, nom: "Red Dead Redemption II", prix: "18€", categorie: "Action-Aventure", vignette: "https://image.api.playstation.com/cdn/UP1004/CUSA03041_00/Hpl5MtwQgOVF9vJqlfui6SDB5Jl4oBSq.png" }
  ]);

  return (
    <SectionJeuxVideo jeux={jeux} />
  );
}
